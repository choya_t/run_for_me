//
//  SecondViewController.swift
//  DevilSmartLock
//
//  Created by 挺屋友幹 on 2020/10/22.
//  Copyright © 2020 吉田拓人. All rights reserved.
//

import Foundation
import UIKit

class SecondViewController: UIViewController{
    @IBOutlet weak var inputtedTextLabel: UILabel!
    
    var inputtedText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // inputtedTextLabelの表示をinputtedTextの値にする
        self.inputtedTextLabel.text = self.inputtedText
    }
    

}
