//
//  ViewController_setting.swift
//  DevilSmartLock
//
//  Created by 挺屋友幹 on 2020/10/23.
//  Copyright © 2020 吉田拓人. All rights reserved.
//

import UIKit

class ViewController_setting: UIViewController {
    
    @IBOutlet weak var d_test: UILabel!
    @IBOutlet weak var l_test: UILabel!
    
    @IBAction func distance(_ sender: UISlider) {
        sender.setValue(sender.value.rounded(.down), animated: false)
        d_test.text = String(Int(sender.value))
    }
    @IBAction func load(_ sender: UISlider) {
        sender.setValue(sender.value.rounded(.down), animated: false)
        l_test.text = String(Int(sender.value))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}
