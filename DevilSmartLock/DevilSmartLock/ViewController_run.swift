//
//  ViewController_run.swift
//  DevilSmartLock
//
//  Created by 挺屋友幹 on 2020/10/23.
//  Copyright © 2020 吉田拓人. All rights reserved.
//

import UIKit

class ViewController_run: UIViewController {
    
    @IBOutlet weak var unlockButton: UIButton!
    @IBOutlet weak var enabledFalseButton: UIButton!
    @IBOutlet weak var heartRate: UILabel!
    @IBOutlet weak var peakHeartRate: UILabel!
    @IBOutlet weak var progress: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        unlockButton.isHidden = true
        enabledFalseButton.isEnabled = false
        enabledFalseButton.layer.borderColor = UIColor.systemGray2.cgColor
        progress.text = String(0)
        heartRate.text = String(0)
        peakHeartRate.text = String(0)
        progressView.setProgress(0.0, animated: false)
    }
    
   
    @IBAction func testTap(_ sender: Any) {
        unlockButton.isHidden = false
    }
    
}
